Zoom Movie Macro for ImageJ 
====================

This is an updated version of Zoom movie (stack) creator for imageJ  
v2 by Eugene Katrukha katpyxa at gmail.com  
v2a Andrey Aristov: aaristov at pasteur.fr  
v2b (this version) Lachlan Whitehead: whitehead at wehi.edu.au  


#Usage and Features 
##For Still Images: 
* Before running the macro, create an ROI around region to zoom in on
* Run Macro, roi will resize to match the aspect ratio of the original image. If roi is no longer acceptable cancel the macro and move the now resized ROI to the appropriate location
* Select how many frames to zoom in for (more frames = smoother zoom) 
* Optionally Add static frames to the end of the movie 
* Option to zoom in to min or max of roi size - possibly deprecated
* Set size of output movie - for large images you might just want to zoom into 1:1 scaling so select "Same as ROI" otherwise input below
* Set pixel scaling and add scalebar 
	
##For Time Series 
* As above with extra options:
* Optionally append any remaining frames post-zoom
* Select which frame to begin the zoom on
* Optionally play the time during zoom, if unselected time will freeze for duration of zoom in

#Installation
* Click on the zoom_macro_imageJ_v2b.ijm link above
* On the right of the text display, right click on "Raw" and select "Save Link As" to download the macro
* Either install macro or simply open in imagje macro editor and click run

	
